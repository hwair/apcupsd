# README #

This repository is a fork of APCUPSD project.

For more information visit http://www.apcupsd.org

# Background #
This repository is created with the intent to add a minimal number of patches to compile the code under MINGW/MSYS on Windows.

A secondary goal is the addition of a method to trigger killpower from apccontrol. Please have a look at the following notes before using this code.

## DISCLAIMER ##

There is already a method to trigger a killpower by using apccontrol. It kills apcupsd service and recalls it by using the killpower option.
This is the preferred method. The option to send killpower request by using apccontrol is needed only if an userspace application should be available to trigger a UPS killpower in case of power down.

However, this means that killpower can be generated ONLY by a script/process that has the same privileges of APCUPSD.

There are situations where we need to trigger a system shutdown from an application that resides in the user space or with less privileges that apcupsd daemon, and ensure that UPS will enter hibernation. The least potentially harmful choice I was able to work out is to check against the existence of a powerdown file in a location specified by apcupsd.conf and, if the file exists, trigger a power down and a hibernation request.

You may understand that this option is potentially dangerous or harmful if the PC does not switch off within the killpower grace time. If your system supports it, you may modify a shutdown script to ensure that killpower is called when on battery and in the latter shutdown steps. This is the approach followed by some Linux distributions I've seen, however it's not always applicable when using parallel init scripts (systemd).

Note also that, to avoid security flaws, you must ensure that the access restrictions to the directory where the powerdown file is written are properly set.

# BUILDING #

The following is a short set of details about the steps needed to build the sources under Windows.

* Install MSYS. In this guide I'm using MSYS2, but the process should be almost identical. Make sure compilers (gcc g++) and thread library is installed.
* Clone the repository (have a look at the original SVN repo or this GIT repo for commands)
* In order to be able to compile modbus driver, add the _POSIX_THREAD_SAFE_FUNCTIONS to the compiler preprocessor definitions:

   export CPPFLAGS='-D_POSIX_THREAD_SAFE_FUNCTIONS -DHAVE_LOCALTIME_R'
   
The second definition is needed to overcome an issue of autotools with mingw; it seems it's not able to detect localtime_r function. HAVE_LOCALTIME_R preprocessor macro ensures that this function is not redefined.

* Configure for an in-source build. From within the MSYS terminal, type

   ./configure --enable-usb

There are several different configuration options, well described in apcupsd manual on author's website (http://www.apcupsd.org/manual/manual.html#configure-options). You may want a different configuration setup.

* Now run make. Have a look at the patches to solve a number of compilation issues that may occour.

## More details about the change ##

Configuration option under apcupsd.conf is PWROFFPATH
If this option is not set or empty the behavoir is disabled.

```
#!conf

# PWROFFDIR <path to poweroff directory>
#  Directory in which to write the poweroff flag file. This file\n
#  is a user created file that instructs the system to start a poweroff sequence.\n
#  In order to power off the UPS too, apcupsd must be started with --kill-on-powerfail\n
#  or the shutdown scripts should be checked.\n
PWROFFDIR /path/to/directory

```

 